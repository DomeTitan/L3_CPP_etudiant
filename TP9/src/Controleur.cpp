#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::string Controleur::getTexte(){
    std::ostringstream ostringstream;
    _inventaire.trier();
    ostringstream << _inventaire;
    return ostringstream.str();
}

void Controleur::chargerInventaire(std::string nomFichier){
    std::ifstream myfile(nomFichier);
    myfile >> _inventaire;
    actualiser();
}

void Controleur::actualiser(){
    for (auto & v : _vues){
        v->actualiser();
    }
}