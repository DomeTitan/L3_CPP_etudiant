//
// Created by quentin on 30/04/2020.
//

#include <algorithm>
#include "Stock.h"

void Stock::recalculerStock(const Inventaire & inventaire){
    std::for_each(inventaire._bouteilles.begin(), inventaire._bouteilles.end(), [this, &inventaire](Bouteille bouteille){
        std::map<std::string, float>::iterator it = _produits.find((bouteille._nom));

        if(it != _produits.end()){
            it->second += bouteille._volume;
        } else{
            _produits.insert(std::pair<std::string, float>(bouteille._nom, bouteille._volume));
        }
    });
}