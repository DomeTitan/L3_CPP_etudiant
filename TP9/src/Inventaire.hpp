#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>
#include <list>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::list<Bouteille> _bouteilles;

    friend std::ostream &operator<<(std::ostream &os, const Inventaire &inventaire);
    friend std::istream &operator>>(std::istream &is, Inventaire &inventaire);
    void trier();
};

#endif
