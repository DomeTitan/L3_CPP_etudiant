//
// Created by quentin on 30/04/2020.
//

#ifndef TP8_STOCK_H
#define TP8_STOCK_H


#include <map>
#include "Inventaire.hpp"

class Stock {
private:
    std::map<std::string, float> _produits;

public:
    void recalculerStock(const Inventaire & inventaire);
};


#endif //TP8_STOCK_H
