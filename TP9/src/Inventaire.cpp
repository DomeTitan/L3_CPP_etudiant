#include <sstream>
#include <algorithm>
#include "Inventaire.hpp"

std::ostream &operator<<(std::ostream &os, const Inventaire &inventaire) {
    std::for_each(inventaire._bouteilles.begin(), inventaire._bouteilles.end(), [&os](const auto &bouteille) {
        os << bouteille;
    });
    return os;
}

std::istream &operator>>(std::istream &is, Inventaire &inventaire){
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    std::string buffer;

    Bouteille b;
    while (std::getline(is, buffer, '\n')){
        std::istringstream istr(buffer);
        istr >> b;
        inventaire._bouteilles.push_back(b);
    }
    std::locale::global(vieuxLoc);

    return is;
}

void Inventaire::trier() {
    _bouteilles.sort();
}