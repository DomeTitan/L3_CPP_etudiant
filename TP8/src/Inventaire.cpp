#include "Inventaire.hpp"

std::ostream &operator<<(std::ostream &os, const Inventaire &inventaire) {
    for(const Bouteille bouteille : inventaire._bouteilles){
        os << bouteille;
    }
    return os;
}
