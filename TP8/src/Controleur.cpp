#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));

    for (auto & v : _vues){
        v->actualiser();
    }
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::string Controleur::getTexte(){
    std::ostringstream ostringstream;
    ostringstream << _inventaire;
    return ostringstream.str();
}

void Controleur::chargerInventaire(std::string nomFichier){
    std::ifstream myfile(nomFichier);

    if(myfile.is_open()){
        std::string line;
        Bouteille bouteille;

        while (getline(myfile, line)){
            myfile >> bouteille;
            _inventaire._bouteilles.push_back(bouteille);
        }
    }

    for (auto & v : _vues){
        v->actualiser();
    }
}