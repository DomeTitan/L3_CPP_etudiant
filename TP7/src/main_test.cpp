//
// Created by quentin on 09/04/2020.
//

#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, char ** argv)
{
    MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
    return CommandLineTestRunner::RunAllTests(argc, argv);
}