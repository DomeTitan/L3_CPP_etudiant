//
// Created by quentin on 09/04/2020.
//

#include <iostream>
#include <fstream>
#include "Image.h"
#include <math.h>

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur) {
    _pixels = new int [_largeur * _hauteur];
    for(int i = 0; i < _largeur * _hauteur; i++){
        _pixels[i] = i;
    }
}

Image::Image(Image const &img){
    _largeur = img.getLargeur();
    _hauteur = img.getHauteur();

    _pixels = new int [_largeur * _hauteur];
    for(int i = 0; i < _largeur; ++i){
        for (int j = 0; j < _hauteur; ++j) {
            setPixel(i, j, img.getPixel(i, j));
        }
    }
}

int Image::getLargeur() const {
    return _largeur;
}

int Image::getHauteur() const {
    return _hauteur;
}

int& Image::getPixel(int i, int j) {
    return _pixels[_largeur * j + i];
}

int Image::getPixel(int i, int j) const{
    return _pixels[_largeur * j + i];
}

void Image::setPixel(int i, int j, int couleur){
    _pixels[_largeur * j + i] = couleur;
}

Image::~Image() {
    delete[] _pixels;
}

Image& Image::operator=(const Image& image){
    _largeur = image.getLargeur();
    _hauteur = image.getHauteur();
    delete[] _pixels;
    _pixels = new int [_largeur * _hauteur];

    for(int i = 0; i < _largeur; ++i){
        for (int j = 0; j < _hauteur; ++j) {
            setPixel(i, j, image.getPixel(i, j));
        }
    }

    return *this;
}

void ecrirePnm(const Image & img, const std::string & nomFichier){
    std::ofstream myfile;
    myfile.open (nomFichier);
    if (myfile.is_open()) {
        myfile << "P2 " << img.getLargeur() << " " << img.getHauteur() << " 255" << std::endl;
        for(int i = 0; i < img.getLargeur(); ++i){
            for (int j = 0; j < img.getHauteur(); ++j) {
                myfile << img.getPixel(j, i) << " ";
            }
            myfile << std::endl;
        }
        myfile.close();
    } else {
        std::cout << "Le fichier ne s'est pas ouvert." << std::endl;
    }
}

void remplir(Image & img) {
    for(int i = 0; i < img.getLargeur(); i++)
    {
        double a = ((cos(i)+1)/2)*255;
        for(int j = 0; j < img.getHauteur(); j++)
        {
            img.setPixel(i, j, a);
        }
    }
}

Image bordure(const Image & img, int couleur, int epaisseur){
    Image image(img);
    for(int i = 0; i < image.getLargeur(); ++i){
        for (int j = 0; j < epaisseur; ++j) {
            image.setPixel(i, j, couleur);
        }
        for (int j = image.getHauteur() - epaisseur; j < image.getHauteur(); ++j) {
            image.setPixel(i, j, couleur);
        }
    }

    for(int i = 0; i < image.getHauteur(); ++i){
        for (int j = 0; j < epaisseur; ++j) {
            image.setPixel(j, i, couleur);
        }
        for (int j = image.getLargeur() - epaisseur; j < image.getLargeur(); ++j) {
            image.setPixel(j, i, couleur);
        }
    }
    return image;
}