//
// Created by quentin on 09/04/2020.
//

#include "Image.h"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Bibliotheque_trierParAuteurEtTitre_1)
{
    Image image (6, 5);
    CHECK(image.getLargeur() == 6);
    CHECK(image.getHauteur() == 5);
    CHECK(image.getPixel(0, 0) == 0);
    image.getPixel(0, 0) = 5;
    CHECK(image.getPixel(0, 0) == 5);
}
