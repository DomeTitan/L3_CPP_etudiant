//
// Created by quentin on 09/04/2020.
//

#ifndef TP_VIDE_IMAGE_H
#define TP_VIDE_IMAGE_H


class Image {
private:
    int _largeur;
    int _hauteur;
    int* _pixels;
public:
    Image(int largeur, int hauteur);
    Image(Image const &img);
    virtual ~Image();
    int getLargeur() const;
    int getHauteur() const;
    int& getPixel(int i, int j);
    int getPixel(int i, int j) const;
    void setPixel(int i, int j, int couleur);
    Image& operator=(const Image& image);
};

void ecrirePnm(const Image & img, const std::string & nomFichier);
void remplir(Image & img);
Image bordure(const Image & img, int couleur, int epaisseur);

#endif //TP_VIDE_IMAGE_H
