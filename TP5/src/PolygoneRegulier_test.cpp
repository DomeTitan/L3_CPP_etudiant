#include <CppUTest/CommandLineTestRunner.h>
#include "Couleur.h"
#include "Point.h"
#include "PolygoneRegulier.h"

TEST_GROUP(GroupPolygoneRegulier) {
};

TEST(GroupPolygoneRegulier, test_polygoneRegulier_1) {
    PolygoneRegulier polygoneRegulier(Couleur(0.0, 1.0, 0.0), Point(100, 200), 50, 5);

    CHECK_EQUAL(Couleur(0.0, 1.0, 0.0)._r, polygoneRegulier.getCouleur()._r);
    CHECK_EQUAL(Couleur(0.0, 1.0, 0.0)._g, polygoneRegulier.getCouleur()._g);
    CHECK_EQUAL(Couleur(0.0, 1.0, 0.0)._b, polygoneRegulier.getCouleur()._b);

    CHECK_EQUAL(5, polygoneRegulier.getNbPoints());
    //CHECK_EQUAL(5, polygoneRegulier.getPoints().length);

    CHECK_EQUAL(Point(150, 200)._x, polygoneRegulier.getPoints(0)->_x);
    CHECK_EQUAL(Point(150, 200)._y, polygoneRegulier.getPoints(0)->_y);

    CHECK_EQUAL(Point(115, 248)._x, polygoneRegulier.getPoints(1)->_x);
    CHECK_EQUAL(Point(115, 248)._y, polygoneRegulier.getPoints(1)->_y);

    CHECK_EQUAL(Point(60, 229)._x, polygoneRegulier.getPoints(2)->_x);
    CHECK_EQUAL(Point(60, 229)._y, polygoneRegulier.getPoints(2)->_y);

    CHECK_EQUAL(Point(60, 171)._x, polygoneRegulier.getPoints(3)->_x);
    CHECK_EQUAL(Point(60, 171)._y, polygoneRegulier.getPoints(3)->_y);

    CHECK_EQUAL(Point(115, 152)._x, polygoneRegulier.getPoints(4)->_x);
    CHECK_EQUAL(Point(115, 152)._y, polygoneRegulier.getPoints(4)->_y);
};