//
// Created by quentin on 23/03/2020.
//

#ifndef TP4_POLYGONEREGULIER_H
#define TP4_POLYGONEREGULIER_H


#include <vector>
#include "Point.h"
#include "FigureGeometrique.h"

class PolygoneRegulier : public FigureGeometrique{
private:
    int _nbPoints;
    std::vector<Point> _points;

public:
    PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes);
    virtual void afficher(const Cairo::RefPtr<Cairo::Context> & context) const override;
    int getNbPoints() const;
    const Point &getPoint(int indice) const;
};


#endif //TP4_POLYGONEREGULIER_H
