#include <iostream>
#include <vector>
#include "Ligne.h"
#include "PolygoneRegulier.h"
#include "ViewerFigures.h"

#include <gtkmm.h>

int main(int argc, char **argv) {

    ViewerFigures viewerFigures(argc, argv);
    viewerFigures.run();

    return 0;
}
