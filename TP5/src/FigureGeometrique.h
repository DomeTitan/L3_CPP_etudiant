//
// Created by quentin on 23/03/2020.
//

#ifndef TP4_FIGUREGEOMETRIQUE_H
#define TP4_FIGUREGEOMETRIQUE_H


#include <cairomm/context.h>
#include "Couleur.h"

class FigureGeometrique {
protected:
    Couleur _couleur;

public:
    FigureGeometrique(const Couleur &couleur);
    const Couleur &getCouleur() const;
    virtual void afficher(const Cairo::RefPtr<Cairo::Context> & context) const;
    virtual ~FigureGeometrique(){};

};


#endif //TP4_FIGUREGEOMETRIQUE_H
