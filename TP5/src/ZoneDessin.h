//
// Created by quentin on 09/04/2020.
//

#ifndef L3_CPP_TP5_ZONEDESSIN_H
#define L3_CPP_TP5_ZONEDESSIN_H


#include <gtkmm/drawingarea.h>
#include "FigureGeometrique.h"

class ZoneDessin : public Gtk::DrawingArea{
protected:
    std::vector<FigureGeometrique*> _figures;
    bool on_draw(const Cairo::RefPtr<Cairo::Context> & contexte) override;
    bool gererClic(GdkEventButton* event);
public:
    ZoneDessin();
    virtual ~ZoneDessin();
};


#endif //L3_CPP_TP5_ZONEDESSIN_H
