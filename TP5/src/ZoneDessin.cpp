//
// Created by quentin on 09/04/2020.
//

#include <iostream>
#include "ZoneDessin.h"
#include "FigureGeometrique.h"
#include "Point.h"
#include "Ligne.h"
#include "PolygoneRegulier.h"

ZoneDessin::ZoneDessin() {
    _figures = {
            new Ligne(Couleur(1, 0, 0), Point(0, 0), Point(100, 200)),
            new PolygoneRegulier(Couleur(0, 1, 0), Point(300, 200), 50, 5)
    };

    add_events(Gdk::BUTTON_PRESS_MASK);
    signal_button_press_event().connect(sigc::mem_fun(*this, &ZoneDessin::gererClic));
};

ZoneDessin::~ZoneDessin() {
    for (FigureGeometrique *fG : _figures) {
        delete fG;
    }
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & contexte) {
    for(FigureGeometrique *fg : _figures){
        fg->afficher(contexte);
    }
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton* event){
    if(event->type == GDK_BUTTON_PRESS){

        if(event->button == 1){
            float c_r = std::rand()%101/100.0f;
            float c_g = std::rand()%101/100.0f;
            float c_b = std::rand()%101/100.0f;
            Point center(event->x, event->y);
            _figures.push_back(new PolygoneRegulier(Couleur(c_r, c_g, c_b), center, rand()%100, rand()%10));
        }

        if(event->button == 3){
            FigureGeometrique* fg = _figures.back();
            _figures.pop_back();
            delete fg;
        }

        get_window()->invalidate(false);
    }
    return true;
}