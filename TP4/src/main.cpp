#include <iostream>
#include <vector>
#include "Ligne.h"
#include "PolygoneRegulier.h"

int main() {

    Point p1 (100, 200);
    Point p2;
    Couleur rouge (1.0, 0.0, 0.0);
    Couleur vert (0.0, 1.0, 0.0);
    std::vector<FigureGeometrique*> vector{
            new Ligne (rouge, p2, p1),
            new PolygoneRegulier (vert, p1, 50, 5),
            new PolygoneRegulier (vert, p2, 10, 4)
    };

    for(FigureGeometrique *ptrFigureGeometrique : vector) {
        ptrFigureGeometrique->afficher();
    }

    for(FigureGeometrique *ptrFigureGeometrique : vector) {
        delete ptrFigureGeometrique;
    }

    return 0;
}
