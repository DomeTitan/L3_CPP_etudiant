//
// Created by quentin on 23/03/2020.
//

#ifndef TP4_LIGNE_H
#define TP4_LIGNE_H


#include "Point.h"
#include "FigureGeometrique.h"

class Ligne : public FigureGeometrique{
private:
    Point _p0;
    Point _p1;

public:
    Ligne(const Couleur &couleur, const Point &p0, const Point &p1);
    virtual ~Ligne(){}
    const Point &getP0() const;
    const Point &getP1() const;
    virtual void afficher() const override;
};


#endif //TP4_LIGNE_H
