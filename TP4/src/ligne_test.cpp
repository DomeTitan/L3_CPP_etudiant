#include <CppUTest/CommandLineTestRunner.h>
#include "Ligne.h"
#include "Couleur.h"

TEST_GROUP(GroupLigne) {
};

TEST(GroupLigne, test_ligne_1) {
    Ligne l (Couleur(1, 0, 0), Point(0, 0), Point(100, 200));
    CHECK_EQUAL(Point(0, 0)._x, l.getP0()._x);
    CHECK_EQUAL(Point(0, 0)._y, l.getP0()._y);
    CHECK_EQUAL(Point(100, 200)._x, l.getP1()._x);
    CHECK_EQUAL(Point(100, 200)._y, l.getP1()._y);
};