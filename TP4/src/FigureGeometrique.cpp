//
// Created by quentin on 23/03/2020.
//

#include <iostream>
#include "FigureGeometrique.h"

FigureGeometrique::FigureGeometrique(const Couleur &couleur) : _couleur(couleur) {}

const Couleur &FigureGeometrique::getCouleur() const {
    return _couleur;
}

void FigureGeometrique::afficher() const{}
