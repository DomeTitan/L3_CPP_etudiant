//
// Created by quentin on 23/03/2020.
//

#include <iostream>
#include <cmath>
#include "PolygoneRegulier.h"
#include "Point.h"

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur,Point const& centre, int rayon, int nbCotes) : FigureGeometrique(couleur), _nbPoints(nbCotes) {
    _points = new Point[nbCotes];

    for (int i=0; i<_nbPoints; i++) {
        float theta = i*2*M_PI/(float)_nbPoints;
        double x = centre._x + rayon * cos(theta);
        double y = centre._y + rayon * sin(theta);
        _points[i] = {static_cast<int>(round(x)), static_cast<int>(round(y))};
    }
}

PolygoneRegulier::~PolygoneRegulier(){
    delete[] _points;
}

void PolygoneRegulier::afficher() const{
    std::cout << "PolygoneRegulier " << _couleur._r << "_" << _couleur._g << "_" << _couleur._b << " ";

    for (unsigned i = 0;  i < _nbPoints; i++) {
        std::cout << _points[i]._x << "_" << _points[i]._y << " ";
    }
    std::endl(std::cout);
}

int PolygoneRegulier::getNbPoints() const {
    return _nbPoints;
}

Point *PolygoneRegulier::getPoints(int indice) const {
    return &_points[indice];
}
