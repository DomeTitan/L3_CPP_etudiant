//
// Created by quentin on 23/03/2020.
//

#include <iostream>
#include "Ligne.h"


Ligne::Ligne(const Couleur &couleur, const Point &p0, const Point &p1) : FigureGeometrique(couleur), _p0(p0), _p1(p1) {}

const Point &Ligne::getP0() const {
    return _p0;
}

const Point &Ligne::getP1() const {
    return _p1;
}

void Ligne::afficher() const{
    //std::cout << "Ligne " << getCouleur()._r << "_" << getCouleur()._g << "_" << getCouleur()._b << " " << _p0._x << "_" << _p0._y << " " << _p1._x << "_" << _p1._y << std::endl;
    std::cout << "Ligne " << _couleur._r << "_" << _couleur._g << "_" << _couleur._b << " " << _p0._x << "_" << _p0._y << " " << _p1._x << "_" << _p1._y << std::endl;
}
