//
// Created by quentin on 27/03/2020.
//

#include <iostream>
#include "Livre.h"

Livre::Livre() {}

Livre::Livre(const std::string &titre, const std::string &auteur, int annee) : _annee(annee) {
    /* test du titre */
    if(inString(titre, ";")){
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }
    if(inString(titre, "\n")){
        throw std::string("erreur : titre non valide ('\n' non autorisé)");
    }

    /* test de l'auteur */
    if(inString(auteur, ";")){
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    }
    if(inString(auteur, "\n")){
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    }
    _titre = titre;
    _auteur = auteur;
}

const std::string &Livre::getTitre() const {
    return _titre;
}

const std::string &Livre::getAuteur() const {
    return _auteur;
}

int Livre::getAnnee() const {
    return _annee;
}

bool Livre::inString(const std::string& s, const std::string& needle){
    if(s.find(needle) == std::string::npos){
        return false;
    }
    return true;
}

bool Livre::operator<(Livre l2){
    if(this->_auteur == l2._auteur){
        return this->_titre < l2._titre;
    } else {
        return this->_auteur < l2._auteur;
    }
}

bool Livre::operator==(Livre l2){
    if(this->_auteur == l2._auteur && this->_titre == l2._titre && this->_annee == l2._annee){
        return true;
    }
    return false;
}

std::ostream &operator<<(std::ostream &os, const Livre &livre) {
    os << livre._titre << ";" << livre._auteur << ";" << livre._annee;
    return os;
}

std::istream &operator>>(std::istream &is, Livre &livre){
    int size = 99;
    char str[size];

    is.getline(str, size, ';');
    livre._titre = str;

    is.getline(str, size, ';');
    livre._auteur = str;

    is.getline(str, size, ';');
    livre._annee = std::atoi(str);
}