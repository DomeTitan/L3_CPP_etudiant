//
// Created by quentin on 27/03/2020.
//

#include <iostream>
#include <algorithm>
#include "Bibliotheque.h"

void Bibliotheque::afficher() const{
    for (Livre l : *this) {
        std::cout << l << std::endl;
    }
}

void Bibliotheque::trierParAuteurEtTitre(){
    std::sort(this->begin(), this->end());
}

void Bibliotheque::trierParAnnee(){
    std::sort(this->begin(), this->end(), [](Livre l1, Livre l2) {
        return l1.getAnnee() < l2.getAnnee();
    });
}

void Bibliotheque::ecrireFichier(const std::string &nameFile){
    std::ofstream myfile;
    myfile.open (nameFile);
    if (myfile.is_open()) {
        for (Livre l : *this) {
            myfile << l << std::endl;
        }
        myfile.close();
    } else {
        std::cout << "Le fichier ne s'est pas ouvert." << std::endl;
    }
}

void Bibliotheque::lireFichier(const std::string &nameFile){
    std::string line;

    std::ifstream myfile (nameFile);
    if (myfile.is_open())
    {
        char delimiter = ';';
        while (getline (myfile,line))
        {
            size_t pos = 0;
            std::string elements[2];
            while ((pos = line.find(delimiter)) != std::string::npos) {
                // On récupère se qu'il y a en les ;
                if(elements->size() == 0){
                    elements[0] = line.substr(0, pos);
                } else{
                    elements[elements->size() - 1] = line.substr(0, pos);
                }
                // on supprime la partie déjà utilisée
                line.erase(0, pos + 1);
            }
            this->push_back(Livre(elements[0],elements[1],std::stoi(line)));
        }
        myfile.close();
    } else{
        throw std::string("erreur : lecture du fichier impossible");
    }
}