
#include <iostream>
#include "Livre.h"
#include "Bibliotheque.h"

int main() {
    //std::cout << "blabla" << std::endl;
    Bibliotheque b1;
    b1.push_back(Livre("t0","a0",42));
    b1.push_back(Livre("t1","a1",13));
    b1.push_back(Livre("t2","a2",37));
    b1.ecrireFichier("bibliotheque_fichier_tmp.txt");
    Bibliotheque b2;
    b2.lireFichier("bibliotheque_fichier_tmp.txt");
    b2.afficher();
    return 0;
}

