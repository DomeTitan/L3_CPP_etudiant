#include <iostream>
#include <cmath>
#include "Vecteur3.hpp"

// implémente la méthode déclarée
void Vecteur3::afficher() const {
    std::cout << "(" << x << ", " << y << ", " << z << ")" << std::endl;
}

float Vecteur3::norme() {
    float value = std::sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    return value;
}

void afficher(Vecteur3 v){
    std::cout << "(" << v.x << ", " << v.y << ", " << v.z << ")" << std::endl;
}

float produitScalaire(Vecteur3 v1, Vecteur3 v2){
    float value = v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
    return value;
}

Vecteur3 addition(Vecteur3 v1, Vecteur3 v2){
    Vecteur3 v = {v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
    return v;
}