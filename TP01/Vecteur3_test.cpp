#include "Vecteur3.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupVecteur3) { };

TEST(GroupVecteur3, test_vecteur3_1) {  // premier test
    Vecteur3 v = {2, 3, 6};
    float value = v.norme();
    CHECK_EQUAL(7, value);
}