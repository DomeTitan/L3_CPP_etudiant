#include "Fibonacci.hpp"

int fibonacciRecursif(int n)
{
    if (n == 1 || n == 0)
    {
        return n;
    }
    else
    {
        return fibonacciRecursif(n - 1) + fibonacciRecursif(n - 2);
    }
    
    
}

int fibonacciIteratif(int n)
{
    int x = 0, y = 1, z = 0;
    for (int i = 0; i < n; i++)
    {
        z = x + y;
        x = y;
        y = z;
    }
    return x;
}