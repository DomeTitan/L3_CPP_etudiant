#include <iostream>
#include "Fibonacci.hpp"
#include "Vecteur3.hpp"

int main()
{
    std::cout << "Hello world!" << std::endl;
    std::cout << fibonacciRecursif(6) << std::endl;
    std::cout << fibonacciIteratif(6) << std::endl;

    Vecteur3 v3 = {2, 3, 6};
    v3.afficher();
    afficher(v3);
    std::cout << v3.norme() << std::endl;


    Vecteur3 v = {2, 3, 6};

    float ps = produitScalaire(v, v3);

    std::cout << ps << std::endl;

    Vecteur3 vTest = addition(v, v3);
    vTest.afficher();
    return 0;
}
