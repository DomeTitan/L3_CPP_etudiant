struct Vecteur3 {
    float x, y, z;

    void afficher() const;   // déclare une méthode
    float norme();
};

void afficher(Vecteur3 v);

float produitScalaire(Vecteur3 v1, Vecteur3 v2);

Vecteur3 addition(Vecteur3 v1, Vecteur3 v2);