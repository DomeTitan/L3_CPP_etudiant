#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) { };

TEST(GroupFibonacci, test_fibonacci_1) {  // premier test
    int result0 = fibonacciRecursif(0);
    CHECK_EQUAL(0, result0);
    int result1 = fibonacciRecursif(1);
    CHECK_EQUAL(1, result1);
    int result2 = fibonacciRecursif(2);
    CHECK_EQUAL(1, result2);
    int result3 = fibonacciRecursif(3);
    CHECK_EQUAL(2, result3);
    int result4 = fibonacciRecursif(4);
    CHECK_EQUAL(3, result4);
}

TEST(GroupFibonacci, test_fibonacci_2) {  // deuxième test
    int result0 = fibonacciIteratif(0);
    CHECK_EQUAL(0, result0);
    int result1 = fibonacciIteratif(1);
    CHECK_EQUAL(1, result1);
    int result2 = fibonacciIteratif(2);
    CHECK_EQUAL(1, result2);
    int result3 = fibonacciIteratif(3);
    CHECK_EQUAL(2, result3);
    int result4 = fibonacciIteratif(4);
    CHECK_EQUAL(3, result4);
}