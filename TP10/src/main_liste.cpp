#include <iostream>

#include "Liste.hpp"
#include "ListeGenerique.h"
#include "Personne.h"

int main() {

    /* Liste */
    Liste l1;
    l1.push_front(37);
    l1.push_front(13);
    std::cout << l1 << std::endl;

    /* Liste générique */
    ListeGenerique<float> l2;
    ListeGenerique<int> l3;
    l2.push_front(2.0);
    l2.push_front(3.3);
    l3.push_front(10);
    l3.push_front(100);
    std::cout << l2 << std::endl;
    std::cout << l3 << std::endl;

    /* Liste de personnes */
    Personne p1, p2;
    p1._nom = "Quentin";
    p1._age = 21;
    p2._nom = "Fabien";
    p2._age = 18;

    ListeGenerique<Personne> l4;
    l4.push_front(p1);
    l4.push_front(p2);
    std::cout << l4 << std::endl;

    return 0;
}

