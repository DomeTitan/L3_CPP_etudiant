
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
    private:
        struct Noeud {
            int _valeur;
            Noeud* _ptrNoeudSuivant;
        };

        Noeud* _ptrTete;

    public:
        class iterator {
            private:
                Noeud* _ptrNoeudCourant;

            public:
                iterator(Noeud* ptrNoeudCourant){
                    _ptrNoeudCourant = ptrNoeudCourant;
                }

                const iterator & operator++() {
                    _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                int& operator*() const {
                    return _ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator &iter) const {
                    return _ptrNoeudCourant != iter._ptrNoeudCourant;
                }

                friend Liste; 
        };

    public:

        Liste(){
            this->_ptrTete = nullptr;
        }

        ~Liste(){
            clear();
            delete _ptrTete;
        }

        void push_front(int valeur) {
            Noeud* n = new Noeud();

            n->_valeur = valeur;
            n->_ptrNoeudSuivant = _ptrTete;

            this->_ptrTete = n;
        }

        int& front() const {
            return _ptrTete->_valeur;
        }

        void clear() {
            while (_ptrTete != nullptr){
                Noeud* ptrCourant = _ptrTete;
                _ptrTete = _ptrTete->_ptrNoeudSuivant;
                delete ptrCourant;
            }
        }

        bool empty() const {
            return _ptrTete == nullptr;
        }

        iterator begin() const {
            return _ptrTete;
        }

        iterator end() const {
            return nullptr;
        }

};

std::ostream& operator<<(std::ostream& os, const Liste& l) {
    auto ite = l.begin();
    while (ite != l.end()){
        os << *ite << " ";
        ++ite;
    }
    return os;
}

#endif

