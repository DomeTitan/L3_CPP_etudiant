//
// Created by quentin on 19/05/2020.
//

#ifndef TP10_PERSONNE_H
#define TP10_PERSONNE_H

struct Personne {
    std::string _nom;
    int _age;
};

std::ostream& operator<<(std::ostream& os, const Personne P) {
    return os << P._nom << " " << P._age;
}

#endif //TP10_PERSONNE_H
