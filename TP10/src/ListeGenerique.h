//
// Created by quentin on 19/05/2020.
//

#ifndef TP10_LISTEGENERIQUE_H
#define TP10_LISTEGENERIQUE_H

#include <cassert>
#include <ostream>

template <typename T>
class ListeGenerique {
private:
    struct Noeud {
        T _valeur;
        Noeud* _ptrNoeudSuivant;
    };

    Noeud* _ptrTete;

public:
    class iterator {
    private:
        Noeud* _ptrNoeudCourant;

    public:
        iterator(Noeud* ptrNoeudCourant){
            _ptrNoeudCourant = ptrNoeudCourant;
        }

        const iterator & operator++() {
            _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
            return *this;
        }

        T& operator*() const {
            return _ptrNoeudCourant->_valeur;
        }

        bool operator!=(const iterator &iter) const {
            return _ptrNoeudCourant != iter._ptrNoeudCourant;
        }

        friend ListeGenerique<T>;
    };

public:

    ListeGenerique(){
        this->_ptrTete = nullptr;
    }

    ~ListeGenerique(){
        clear();
        delete _ptrTete;
    }

    void push_front(T valeur) {
        Noeud* n = new Noeud();

        n->_valeur = valeur;
        n->_ptrNoeudSuivant = _ptrTete;

        this->_ptrTete = n;
    }

    T& front() const {
        return _ptrTete->_valeur;
    }

    void clear() {
        while (_ptrTete != nullptr){
            Noeud* ptrCourant = _ptrTete;
            _ptrTete = _ptrTete->_ptrNoeudSuivant;
            delete ptrCourant;
        }
    }

    bool empty() const {
        return _ptrTete == nullptr;
    }

    iterator begin() const {
        return _ptrTete;
    }

    iterator end() const {
        return nullptr;
    }

};

template <typename T>
std::ostream& operator<<(std::ostream& os, const ListeGenerique<T>& l) {
    auto ite = l.begin();
    while (ite != l.end()){
        os << *ite << " ";
        ++ite;
    }
    return os;
}

#endif //TP10_LISTEGENERIQUE_H
