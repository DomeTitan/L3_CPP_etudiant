//
// Created by quentin on 06/03/2020.
//

#include "Client.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, test_client_1) {  // premier test
    Client c (42, "toto");

    CHECK_EQUAL(42, c.getId());
    CHECK_EQUAL("toto", c.getNom());
}
