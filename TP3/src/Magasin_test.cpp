//
// Created by quentin on 06/03/2020.
//
#include "Produit.h"
#include "Magasin.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, test_magasin_1) {  // premier test
    Magasin m;

    m.ajouterClient("toto");
    m.ajouterClient("titi");
    m.ajouterClient("tata");
    CHECK_EQUAL(3, m.nbClients());
    m.supprimerClient(1);
    CHECK_EQUAL(2, m.nbClients());
    CHECK_THROWS(std::string, m.supprimerClient(3));

    m.ajouterProduit("produit1");
    m.ajouterProduit("produit2");
    m.ajouterProduit("produit3");
    CHECK_EQUAL(3, m.nbProduits());
    m.supprimerProduit(0);
    CHECK_EQUAL(2, m.nbProduits());
    CHECK_THROWS(std::string, m.supprimerProduit(3));

    m.ajouterLocation(0, 1);
    m.ajouterLocation(2, 2);
    CHECK_THROWS(std::string,m.ajouterLocation(2, 2));
    CHECK_EQUAL(2, m.nbLocation());
    m.supprimeLocation(2, 2);
    CHECK_EQUAL(1, m.nbLocation());
    CHECK_THROWS(std::string,m.supprimeLocation(3, 3));
    CHECK_TRUE(m.trouverClientDansLocation(0));
    CHECK_FALSE(m.trouverClientDansLocation(2));
    CHECK_TRUE(m.trouverProduitDansLocation(1));
    CHECK_FALSE(m.trouverProduitDansLocation(2));

    std::vector<int> tempClient = m.calculerClientsLibres();
    std::vector<int> tempProduit = m.calculerProduitsLibres();
    CHECK_EQUAL(1, tempClient.size());
    CHECK_EQUAL(2, tempClient[1]);
    CHECK_EQUAL(1, tempProduit.size());
    CHECK_EQUAL(2, tempProduit[1]);
}
