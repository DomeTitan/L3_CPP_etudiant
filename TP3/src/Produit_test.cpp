//
// Created by quentin on 06/03/2020.
//
#include "Produit.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, test_produit_1) {  // premier test
    Produit p (42, "titi");

    CHECK_EQUAL(42, p.getId());
    CHECK_EQUAL("titi", p.getDescription());
}
