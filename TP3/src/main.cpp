#include <iostream>
#include "Location.hpp"
#include "Client.h"
#include "Produit.h"
#include "Magasin.h"

int main() {
    /*Location location;
    location.afficherLocation();

    Client c (42, "toto");
    c.affichagerClient();

    Produit p (42, "titi");
    p.afficherProduit();*/

    Magasin m;

    m.ajouterClient("toto");
    m.ajouterClient("titi");
    m.ajouterClient("tata");
    std::cout << "Nb client: " << m.nbClients() << std::endl;
    m.afficherClients();
    m.supprimerClient(1);
    std::cout << "Nb client: " << m.nbClients() << std::endl;
    m.afficherClients();
    m.supprimerClient(3);

    m.ajouterProduit("produit1");
    m.ajouterProduit("produit2");
    m.ajouterProduit("produit3");
    std::cout << "Nb produits: " << m.nbProduits() << std::endl;
    m.afficherProduits();
    m.supprimerProduit(0);
    std::cout << "Nb produits: " << m.nbProduits() << std::endl;
    m.afficherProduits();
    m.supprimerProduit(3);

    m.ajouterLocation(0, 1);
    m.ajouterLocation(2, 2);
    m.ajouterLocation(2, 2);
    std::cout << "Nb locations: " << m.nbLocation() << std::endl;
    m.afficherLocations();
    m.supprimeLocation(2, 2);
    std::cout << "Nb locations: " << m.nbLocation() << std::endl;
    m.afficherLocations();
    m.supprimeLocation(3, 3);
    std::cout << m.trouverClientDansLocation(0) << std::endl;
    std::cout << m.trouverClientDansLocation(2) << std::endl;
    std::cout << m.trouverProduitDansLocation(1) << std::endl;
    std::cout << m.trouverProduitDansLocation(2) << std::endl;

    std::vector<int> tempClient = m.calculerClientsLibres();
    std::vector<int> tempProduit = m.calculerProduitsLibres();

    if(tempClient.size() != 0) {
        for (size_t i = 0; i <= tempClient.size() - 1; ++i) {
            std::cout << "Client n°" << tempClient[i] << std::endl;
        }
    }
    else{
        std::cout << "Aucun client n'est libre" << std::endl;
    }

    if(tempProduit.size() != 0) {
        for (size_t i = 0; i <= tempProduit.size() - 1; ++i) {
            std::cout << "Produit n°" << tempProduit[i] << std::endl;
        }
    }
    else{
        std::cout << "Aucun produit n'est libre" << std::endl;
    }

    return 0;
}
