//
// Created by quentin on 06/03/2020.
//

#include <iostream>
#include "Magasin.h"
#include "Client.h"

Magasin::Magasin() {
    _idCourantClient = 0;
    _idCourantProduit = 0;
}

const int Magasin::nbClients(){
    return _clients.size();
}

void Magasin::ajouterClient(const std::string &nom){
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient++;
}

const void Magasin::afficherClients(){
    for (size_t i = 0; i < Magasin::nbClients(); ++i) {
        _clients[i].Client::affichagerClient();
    }
}

void Magasin::supprimerClient(int idClient){
    try {
        for (size_t i = 0; i < nbClients()-1; ++i) {
            if (_clients[i].getId() == idClient){
                std::swap(_clients[i], _clients[i+1]);
            }
        }
        if(_clients[nbClients() - 1].getId() == idClient){
            _clients.pop_back();
        } else{
            throw std::string("ERREUR: ce client n'existe pas");
        }
    }
    catch(std::string const& chaine){
        std::cout << chaine << std::endl;
    }
}


const int Magasin::nbProduits(){
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string &nom){
    _produits.push_back(Produit(_idCourantProduit, nom));
    _idCourantProduit++;
}

const void Magasin::afficherProduits(){
    for (size_t i = 0; i < Magasin::nbProduits(); ++i) {
        _produits[i].Produit::afficherProduit();
    }
}

void Magasin::supprimerProduit(int idProduit){
    try {
        for (size_t i = 0; i < nbProduits()-1; ++i) {
            if (_produits[i].getId() == idProduit){
                std::swap(_produits[i], _produits[i+1]);
            }
        }
        if(_produits[nbProduits() - 1].getId() == idProduit){
            _produits.pop_back();
        } else{
            throw std::string("ERREUR: ce produit n'existe pas");
        }
    }
    catch(std::string const& chaine){
        std::cout << chaine << std::endl;
    }
}

const int Magasin::nbLocation(){
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit){
    try {
        Location l(idClient, idProduit);
        if(Magasin::nbLocation() != 0) {
            for (size_t i = 0; i <= Magasin::nbLocation() - 1; ++i) {
                if (l._idProduit == _locations[i]._idProduit && l._idClient == _locations[i]._idProduit) {
                    throw std::string("ERREUR: cette location existe déjà");
                }
            }
            _locations.push_back(l);
        } else{
            _locations.push_back(l);
        }
    }
    catch(std::string const& chaine){
        std::cout << chaine << std::endl;
    }
}

const void Magasin::afficherLocations(){
    for (size_t i = 0; i < Magasin::nbLocation(); ++i) {
        _locations[i].Location::afficherLocation();
    }
}

void Magasin::supprimeLocation(int idClient, int idProduit){
    try {
        Location p (idClient, idProduit);
        for (size_t i = 0; i < Magasin::nbLocation() - 1; ++i) {
            if(p._idProduit == _locations[i]._idProduit && p._idClient == _locations[i]._idProduit){
                std::swap(_locations[i], _locations[i+1]);
            }
        }
        if(p._idProduit == _locations[nbLocation()-1]._idProduit && p._idClient == _locations[nbLocation()-1]._idProduit){
            _locations.pop_back();
        } else{
            throw std::string("ERREUR: cette localisation n'existe pas");
        }
    }
    catch(std::string const& chaine){
        std::cout << chaine << std::endl;
    }
}

const bool Magasin::trouverClientDansLocation(int idClient){
    for (size_t i = 0; i <= Magasin::nbLocation() - 1; ++i) {
        if(_locations[i]._idClient == idClient){
            return true;
        }
    }
    return false;
}

const std::vector<int> Magasin::calculerClientsLibres(){
    std::vector<int> compt;

    for (size_t i = 0; i <= Magasin::nbClients() - 1; ++i) {
        if(!trouverClientDansLocation(_clients[i].getId())){
            compt.push_back(_clients[i].getId());
        }
    }

    return compt;
}

const bool Magasin::trouverProduitDansLocation(int idProduit){
    for (size_t i = 0; i <= Magasin::nbLocation() - 1; ++i) {
        if(_locations[i]._idProduit == idProduit){
            return true;
        }
    }
    return false;
}

const std::vector<int> Magasin::calculerProduitsLibres(){
    std::vector<int> compt;

    for (size_t i = 0; i <= Magasin::nbProduits() - 1; ++i) {
        if(!trouverProduitDansLocation(_produits[i].getId())){
            compt.push_back(_produits[i].getId());
        }
    }

    return compt;
}