//
// Created by quentin on 06/03/2020.
//

#include <iostream>
#include "Location.hpp"


Location::Location(){
    _idClient = 0;
    _idProduit = 2;
}

Location::Location(int idClient, int idProduit){
    this->_idClient = idClient;
    this->_idProduit = idProduit;
}

void Location::afficherLocation() const {
    std::cout << "Location (" << _idClient << ", " << _idProduit << ")" << std::endl;
}