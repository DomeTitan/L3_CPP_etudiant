//
// Created by quentin on 06/03/2020.
//

#include "Liste.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, test_liste_1) {  // premier test
    Liste l;
    CHECK_EQUAL(0, l.getTaille());
    l.ajouterDevant(1);
    CHECK_EQUAL(1, l.getTaille());
    l.ajouterDevant(2);
    CHECK_EQUAL(2, l.getTaille());
    CHECK_EQUAL(2, l.getElement(1));
}
