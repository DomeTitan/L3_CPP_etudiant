#include "Liste.hpp"

Noeud::Noeud(int valeur){
    this->valeur = valeur;
    this->suivant = nullptr;
}

Noeud::~Noeud() {}

Liste::Liste(){
    tete = nullptr;
}

Liste::~Liste() {
    Noeud* current;
    while (tete != nullptr){
         current = tete;
        tete = current->suivant;
        delete current;
    }
}

void Liste::ajouterDevant(int valeur){
    Noeud* nouveau = new Noeud(valeur);

    nouveau->suivant = tete;
    tete = nouveau;
}

int Liste::getTaille() const{
    int compt = 0;
    Noeud* noeud = tete;

    while (noeud != nullptr)
    {
        noeud = noeud->suivant;
        compt++;
    }

    return compt;
}

int Liste::getElement(int indice) const{

    if(indice != 0 && indice <= this->getTaille()){
        int compt = 1;
        Noeud* noeud = tete;

        while (compt != indice)
        {
            noeud = noeud->suivant;
            compt++;
        }
        return noeud->valeur;
    }

    return 0;
}