#include <iostream>
#include "Liste.hpp"

void exo1();

void exo2();

void exo3();

int main(){
    //exo1();
    //exo2();
    exo3();
}

/* Pointeurs et allocation dynamique */
void exo1() {
    int a;
    a = 42;
    int* p;
    p = &a;
    *p = 37;
    std::cout << a << std::endl;

    int* t;
    t = new int [10];
    t[2] = 42;
    delete [] t;
    t = nullptr;
}

/* Implémentation d’une liste chainée */
void exo2(){
    Liste l;
    l.ajouterDevant(1);
    l.ajouterDevant(2);
    std::cout << l.getTaille() << std::endl;
    std::cout << l.getElement(2) << std::endl;
}

/* Analyse des fuites mémoires */
void exo3() {
    Liste l2;
    l2.ajouterDevant(13);
    l2.ajouterDevant(37);
    Noeud* noeud = l2.tete;
    for (int i = 1; i <= l2.getTaille(); ++i) {
        std::cout << noeud->valeur << std::endl;
        noeud = noeud->suivant;
    }
}
